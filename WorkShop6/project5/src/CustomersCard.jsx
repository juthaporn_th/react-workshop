import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Link from "@mui/material/Link";
import "./CustomersCard.css";

export default function CustomersCard() {
  const [items, setItems] = useState([]);

  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("http://172.16.157.2:5000/customers")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const UserDelete = (id) => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "DELETE",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch("http://172.16.157.2:5000/customers/" + id, requestOptions)
      .then((response) => response.text())
      .then((result) => console.log(result))
      .catch((error) => console.log("error", error));
  };

  const UserUpdate = (id) => {
    window.location = "/update/" + id;
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Box className="box">
          <Grid container spacing={2}>
            <Link href="create" sx={{ m: 2 }}>
              <Button variant="contained">Create</Button>
            </Link>
            <Grid item xs={4} spacing={2} sx={{
              display:"flex",
              justifyContent:"center",
              

            }}>
            {items.map((item) => (
        
                <Card sx={{ maxWidth: 345 ,m:2 ,minWidth:200}}  key={item.id}>
                  <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                      ID : {item.id}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      Name : {item.name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      Phone : {item.pjone_number}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                      Date Created : {item.date_created}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button onClick={() => UserUpdate(item.id)} size="small">
                      Edit
                    </Button>
                    <Button onClick={() => UserDelete(item.id)} size="small">
                      Delete
                    </Button>
                  </CardActions>
                </Card>
             
            ))}
             </Grid>
          </Grid>
        </Box>
      </Container>
    </React.Fragment>
  );
}
