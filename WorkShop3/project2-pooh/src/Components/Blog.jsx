import { Link } from "react-router-dom";
import blogs from "../blogs";
import "./Blog.css";
import { useState, useEffect } from "react";

export default function Blogs() {
  const [search, setSearch] = useState("");
  const [filterBlog, setFilterBlog] = useState([]);

  useEffect(() => {
    const result = blogs.filter((item) =>
      item.title.toLowerCase().includes(search.toLowerCase())
    );
    console.log("search = ");
    console.log("result + ");
    console.log(search);
    console.log(result);
    setFilterBlog(result);
  }, [search]);

//   const ages = ["a", "b", "c", "d"];
// const search = "a";

  console.log(filterBlog);

  return (
    <div className="blogs-container">
      <div className="search-container">
        <input
          type="text"
          className="search-input"
          placeholder="Input text"
          value={search}
          onChange={(e) => setSearch(e.target.value)}
        />
      </div>
      <article>
        {filterBlog.map((item) => (
          <Link to={`/blog/${item.id}`} key={item.id}>
            <div className="card">
              <h2>{item.author}</h2>
              <img src={item.image_url} alt="" />
              <p>{item.content}</p>
              <hr />
            </div>
          </Link>
        ))}
      </article>
    </div>
  );
}








