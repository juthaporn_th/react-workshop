import Home from "./components/Home";
import Blogs from "./components/Blogs";
import About from "./components/About";
import Navbar from "./components/Navbar";
import Notfound from "./components/Notfound";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import Details from "./components/Details";

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="About" element={<About />}></Route>
        <Route path="Blogs" element={<Blogs />}></Route>
        <Route path="*" element={<Notfound/>}></Route>
        <Route path="/home" element={<Navigate to="/"/>}></Route>
        <Route path="/info" element={<Navigate to="About"/>}></Route>
        <Route path="/Blogs/:id" element={<Details/>}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
