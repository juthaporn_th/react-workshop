import "./Item.css";
import {FaRegTrashAlt} from "react-icons/fa";
import {AiFillEdit} from "react-icons/ai";
export default function Item(props) {
  const {data,deleteTask,editTask} = props;
  return (
    <>
    <div className="list-item">
    <p>{data.title}</p>
    <div className="button-container">
        <FaRegTrashAlt className="btn" onClick={()=>deleteTask(data.id)}/>
        <AiFillEdit className="btn" onClick={()=>editTask(data.id)}/>
        </div>
    </div>
    </>
  );
}
